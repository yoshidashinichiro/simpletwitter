package chapter6.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;


//アクセス制御のFilterを作成
@WebFilter({"/setting","/edit"})//フィルターの適用範囲を指定
public class LoginFilter implements Filter{

	@Override
	public void doFilter(ServletRequest request , ServletResponse response, FilterChain chain)
			throws IOException, ServletException{

		//HttpServletRequestのrequestとresponseを行うためのキャスト作業（一時的な型変換）
		HttpServletRequest httpRequest = (HttpServletRequest)request;
		HttpServletResponse httpResponse = (HttpServletResponse)response;

		HttpSession session = httpRequest.getSession();
		User user = (User)session.getAttribute("loginUser");

		String servletPath = httpRequest.getServletPath();

		//判定処理
		if(servletPath.equals("/login") || user != null) {
			chain.doFilter(request, response);
		}else {
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("ログインしてください");
			session.setAttribute("errorMessages", errorMessages);
			httpResponse.sendRedirect("./login");
		}
	}

	@Override
	public void init(FilterConfig filterConfig) {//取得
	}

	@Override
	public void destroy() {//解放
	}
}
