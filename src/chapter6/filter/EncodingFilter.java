package chapter6.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;


//doFilter,init,destroyは使用しなくても定義マスト。フィルターの3種の神器。
@WebFilter("/*")//フィルター先を指定
public class EncodingFilter implements Filter {

	public static String INIT_PARAMETER_NAME_ENCODING = "encoding";

	public static String DEFAULT_ENCODING = "UTF-8";

	private String encoding;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		if (request.getCharacterEncoding() == null) {//フィルターを除外するページの記述
			request.setCharacterEncoding(encoding);
		}

		chain.doFilter(request, response); // サーブレットを実行。未記入の場合白紙のページが返る。

		//リグインしているか。セッション領域にログインユーザーの情報があるかないか。ログインユーザの情報を使用して処理を実施
		//if文を用いて、ログインユーザーが空ならログインページへ。
		//リダイレクトループが発生する。ログインページは除外させる。
		//getServletPass()を使用
		//引数のHttpへの型変換が必要
		
		//全ページにかかるログインフィルターを作ります。
		//空のinit,destroyを作成
		//DoFilterでログインしているかしていないかの判断、これからアクセスするのはログインページか、これからのページはログインページ以外でログインしていない時エラー処理をつけて返す
		
		//権限フィルター
	}

	@Override
	public void init(FilterConfig config) {//起動時にコールされる。パラメータの初期化などを実施
		encoding = config.getInitParameter(INIT_PARAMETER_NAME_ENCODING);
		if (encoding == null) {
			System.out.println("EncodingFilter# デフォルトのエンコーディング(UTF-8)を利用します。");
			encoding = DEFAULT_ENCODING;
		} else {
			System.out.println("EncodingFilter# 設定されたエンコーディング(" + encoding
					+ ")を利用します。。");
		}
	}

	@Override
	public void destroy() {//停止時にコールされる。保持しているインスタンスの破棄などを行います。
	}

}
