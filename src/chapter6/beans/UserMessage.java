package chapter6.beans;

import java.io.Serializable;
import java.util.Date;

public class UserMessage implements Serializable {

	//プロパティ
    private int id;
    private String account;
    private String name;
    private int userId;
    private String text;
    private Date created_date;

    //setter
    public void setId(int id) {
    	this.id = id;
    }
    public void setAccount(String account) {
    	this.account = account;
    }
    public void setName(String name) {
    	this.name = name;
    }
    public void setUserId(int userId) {
    	this.userId = userId;
    }
    public void setText(String text) {
    	this.text = text;
    }
    public void setCreated_Date(Date created_date) {
    	this.created_date = created_date;
    }

    //getter
    public int getId() {
    	return this.id;
    }
    public String getAccount() {
    	return this.account;
    }
    public String getName() {
    	return this.name;
    }
    public int getUserId() {
    	return this.userId;
    }
    public String getText() {
    	return this.text;
    }
    public Date getCreated_Date() {
    	return this.created_date;
    }
}