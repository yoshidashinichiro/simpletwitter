package chapter6.beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
	private static final long serialVersionUID  = 1L;

	//プロパティ
    private int id;
    private String account;
    private String name;
    private String email;
    private String password;
    private String description;
    private Date createdDate;
    private Date updatedDate;

    //setter
    public void setId(int id) {
    	this.id = id;
    }
    public void setAccount(String account) {
    	this.account = account;
    }
    public void setName(String name) {
    	this.name = name;
    }
    public void setEmail(String email) {
    	this.email = email;
    }
    public void setPassword(String password) {
    	this.password = password;
    }
    public void setDescription(String description) {
    	this.description = description;
    }
    public void setCreatedDate(Date createdDate) {
    	this.createdDate = createdDate;
    }
    public void setUpdatedDate(Date updatedDate) {
    	this.updatedDate = updatedDate;
    }

    //getter
    public int getId() {
    	return this.id;
    }
    public String getAccount() {
    	return this.account;
    }
    public String getName() {
    	return this.name;
    }
    public String getEmail() {
    	return this.email;
    }
    public String getPassword() {
    	return this.password;
    }
    public String getDescription() {
    	return this.description;
    }
    public Date getCreatedDate() {
    	return this.createdDate;
    }
    public Date getUpdatedDate() {
    	return this.updatedDate;
    }
}