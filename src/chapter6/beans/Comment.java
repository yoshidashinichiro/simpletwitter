package chapter6.beans;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {
	private static final long serialVersionUID  = 1L;
	//プロパティ
	private int id;
	private int messageId;
	private int userId;
	private String text;
	private Date createdDate;
	private Date updatedDate;

	//setter
    public void setId(int id) {
    	this.id = id;
    }
    public void setMessageId(int messageId) {
    	this.messageId = messageId;
    }
    public void setUserId(int userId) {
    	this.userId = userId;
    }
    public void setText(String text) {
    	this.text = text;
    }
    public void setCreatedDate(Date createdDate) {
    	this.createdDate = createdDate;
    }
    public void setUpdatedDate(Date updatedDate) {
    	this.updatedDate = updatedDate;
    }

    //getter
    public int getId() {
    	return this.id;
    }
    public int getMessageId() {
    	return this.messageId;
    }
    public int getUserId() {
    	return this.userId;
    }
    public String getText() {
    	return this.text;
    }
    public Date getCreatedDate() {
    	return this.createdDate;
    }
    public Date getUpdatedDate() {
    	return this.updatedDate;
    }
}