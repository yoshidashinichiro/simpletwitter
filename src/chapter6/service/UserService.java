package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.dao.UserDao;
import chapter6.utils.CipherUtil;

public class UserService {

    public void insert(User user) {//登録画面

        Connection connection = null;
        try {
            // パスワード暗号化
            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            //フレームワークなどを使うことでURLやパスワードなどを設定ファイルに記述することができます。
            connection = getConnection();
            new UserDao().insert(connection, user);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public User select(String accountOrEmail, String password) {//ログイン画面

            Connection connection = null;
            try {
                // パスワード暗号化
                String encPassword = CipherUtil.encrypt(password);

                connection = getConnection();
                User user = new UserDao().select(connection, accountOrEmail, encPassword);
                commit(connection);

                return user;
            } catch (RuntimeException e) {
                rollback(connection);
                throw e;
            } catch (Error e) {
                rollback(connection);
                throw e;
            } finally {
                close(connection);
            }
    }

    public User select(int userId) {//ユーザー情報の取得

        Connection connection = null;
        try {
            connection = getConnection();
            User user = new UserDao().select(connection, userId);
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void update(User user) {//設定画面

        Connection connection = null;
        try {
            connection = getConnection();

            if(!StringUtils.isBlank(user.getPassword())) {
            	String encPassword = CipherUtil.encrypt(user.getPassword());
            	user.setPassword(encPassword);
            }
            new UserDao().update(connection, user);
            commit(connection);

        }catch (RuntimeException e) {
            rollback(connection);
            throw e;
        }catch (Error e) {
            rollback(connection);
            throw e;
        }finally {
            close(connection);
        }
    }
    
    //アカウント入力に関するメソッド追加
    public User select(String account) {

        Connection connection = null;
        try {
            connection = getConnection();
            User user = new UserDao().select(connection, account);
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
 }