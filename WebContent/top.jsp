<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<!-- HTMLのどのバージョン (種類)に準拠したものなのかを示す目的で使用 -->
	<!-- HTML5では文書型宣言は文法的には不要 -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>簡易Twitter</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
        <script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
		<script>
			$(function() {
				$('#delete').on('click', function() {
					alert('消去してよろしいでしょうか');
				});
			});
		</script>
    </head>

    <body>
        <div class="main-contents">
            <div class="header">
			    <c:if test="${ empty loginUser }">
			        <a href="login">ログイン</a>
			        <a href="signup">登録する</a>
			    </c:if>

			    <c:if test="${ not empty loginUser }">
			        <a href="./">ホーム</a>
			        <a href="setting">設定</a>
			        <a href="logout">ログアウト</a>
			    </c:if>
			</div>

			<div class="narrowDown-function">
				<form action = "./" method = "get">
					<input type = "date" name = "startDate" value = "${startDate}" min = "2020-01-01">
					～
					<input type = "date" name = "endDate" value = "${endDate}"><br>
					<input type = "submit" value = "絞込">
				</form>
			</div>

		    <div class="profile">
				<c:if test="${ not empty loginUser }">
			        <div class="name">
			        	<h2>
			        		<c:out value="${loginUser.name}" />
			        	</h2>
			        </div>

			        <div class="account">
			        	@<c:out value="${loginUser.account}" />
			        </div>

			        <div class="description">
			        	<c:out value="${loginUser.description}" />
			        </div>
				</c:if>
		    </div>

		    <div class="errorMessages">
				<c:if test="${ not empty errorMessages }">
			        <ul>
			            <c:forEach items="${errorMessages}" var="errorMessage">
			            <li><c:out value="${errorMessage}" />
			            </c:forEach>
			        </ul>
			    	<c:remove var="errorMessages" scope="session" />
				</c:if>
		    </div>

			<div class="form-area">
			    <c:if test="${ isShowMessageForm }">
			        <form action="message" method="post">
			            いま、どうしてる？<br />
			            <textarea name="text" cols="100" rows="5" class="tweet-box"></textarea><br />
			            <input type="submit" value="つぶやく">（140文字まで）<br>
			        </form>
			    </c:if>
			</div>
			<br>
			<div class="messages">
			    <c:forEach items="${messages}" var="message">
			        <div class="message">
			           <div class="account-name">
			                <span class="account">
			            		<a href="./?user_id=<c:out value="${message.userId}"/> ">
			            			<c:out value="${message.account}" />
			                	</a>
			                </span>

			            	<span class="name">
			            		<c:out value="${message.name}" />
			            	</span>
			            </div>

						<c:forEach var="s" items="${fn:split(message.text, '
')}">
				            <div class="text">
				            	<c:out value="${s}" />
				            </div>
			            </c:forEach>

			            <div class="date">
			            	<fmt:formatDate value="${message.created_Date}" pattern="yyyy/MM/dd HH:mm:ss" />
			            </div>

						<div class = "edit-reply">
							<c:if test="${ loginUser.account == message.account }">
								<div class = "edit">
									<form action = "edit" method = "get">
										<input name = "messageId" value = "${message.id}" type= "hidden"><br>
					        			<input type="submit"  value ="編集"/>
					        		</form>
				        		</div>

				        		<div class = "delete">
					        		<form action = "deleteMessage" method = "post">
					        			<input name="messageId" value="${message.id}" type="hidden">
					        			<input type="submit" value="消去" id="delete"/><br><br>
					        		</form>
					        	</div>
							</c:if>
						</div>

			        	<div class = "comments">
			        		<c:forEach items = "${comments}" var = "comment">
								<c:if test="${ message.id == comment.messageId }">
									<div class = "account-name">
										<span class = "account">
											<c:out value = "${comment.account}"/>
										</span>

										<span class = "name">
											<c:out value = "${comment.name}"/>
										</span>
									</div>

									<c:forEach var="r" items="${fn:split(comment.text, '
')}">
										<div class = "text">
											<c:out value = "${r}"/>
										</div>
									</c:forEach>

									<div class = "date">
										<fmt:formatDate value = "${comment.created_Date}"  pattern = "yyy/MM/dd HH:mm:ss"/>
									</div>
								</c:if>
							</c:forEach>
						</div>

						<div class = "commentform-area">
							<c:if test = "${ isShowMessageForm }">
								<div class = "comment">
									<form action = "comment" method = "post">
										<input name = "messageId" value = "${message.id}" type= "hidden"><br>
										返信<br>
										<textarea name = "comment" cols="90" rows="5" class="comment-box"></textarea><br>
										<input type = "submit" value = "返信"><br><br><br>
									</form>
				        		</div>
			        		</c:if>
			       		</div>
			       </div>
			    </c:forEach>
			</div>
            <div class="copyright"> Copyright(c)ShinichiroYoshida.com</div>
        </div>
    </body>
</html>